-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: rp4
-- Source Schemata: rp4
-- Created: Wed Feb 06 21:06:43 2013
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;;

-- ----------------------------------------------------------------------------
-- Schema rp4
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `rp4` ;
CREATE SCHEMA IF NOT EXISTS `rp4` ;

-- ----------------------------------------------------------------------------
-- Table rp4.campus
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`campus` (
  `idcampus` INT(11) NOT NULL ,
  `campus` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
  PRIMARY KEY (`idcampus`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table rp4.caronista_viagem
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`caronista_viagem` (
  `caronista` VARCHAR(11) NOT NULL ,
  `viagem` INT(11) NOT NULL ,
  PRIMARY KEY (`caronista`, `viagem`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table rp4.cnh
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`cnh` (
  `idcnh` INT(11) NOT NULL ,
  `tipo` VARCHAR(2) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
  PRIMARY KEY (`idcnh`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table rp4.configuracoes
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`configuracoes` (
  `parametro` VARCHAR(45) CHARACTER SET 'utf8' NOT NULL ,
  `valor` VARCHAR(45) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
  PRIMARY KEY (`parametro`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

-- ----------------------------------------------------------------------------
-- Table rp4.cor
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`cor` (
  `idcor` INT(11) NOT NULL ,
  `cor` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcor`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table rp4.motorista
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`motorista` (
  `cpf` VARCHAR(11) NOT NULL ,
  `status` VARCHAR(5) NULL DEFAULT NULL ,
  `numerocnh` VARCHAR(11) NULL DEFAULT NULL ,
  `tipocnh` INT(11) NULL DEFAULT NULL ,
  `datacontr` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`cpf`) ,
  INDEX `cpf_idx` (`cpf` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table rp4.usuario
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`usuario` (
  `cpf` VARCHAR(11) NOT NULL ,
  `login` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(45) NULL DEFAULT NULL ,
  `nome` VARCHAR(45) NULL DEFAULT NULL ,
  `campus` INT(11) NULL DEFAULT NULL ,
  `senha` VARCHAR(45) NULL DEFAULT NULL ,
  `datanasc` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`cpf`, `login`) ,
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC) ,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table rp4.veiculo
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`veiculo` (
  `placa` VARCHAR(7) NOT NULL ,
  `cor` INT(11) NULL DEFAULT NULL ,
  `km` INT(11) NULL DEFAULT NULL ,
  `capacidade` INT(11) NULL DEFAULT NULL ,
  `campus` INT(11) NULL DEFAULT NULL ,
  `ano` INT(11) NULL DEFAULT NULL ,
  `marca` VARCHAR(45) NULL DEFAULT NULL ,
  `modelo` VARCHAR(45) NULL DEFAULT NULL ,
  `status` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`placa`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table rp4.viagem
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `rp4`.`viagem` (
  `id` INT(11) NOT NULL ,
  `origem` INT(11) NULL DEFAULT NULL ,
  `destino` INT(11) NULL DEFAULT NULL ,
  `datasaida` DATETIME NULL DEFAULT NULL ,
  `veiculo` VARCHAR(7) NULL DEFAULT NULL ,
  `motorista` VARCHAR(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
SET FOREIGN_KEY_CHECKS = 1;;

-- ----------------------------------------------------------------------------
-- Inserts
-- ----------------------------------------------------------------------------
INSERT INTO `campus` (`idcampus`, `campus`) VALUES
(5, 'Santana do Livramento'),
(0, 'Alegrete'),
(1, 'Bage'),
(7, 'São Gabriel'),
(8, 'Uruguaiana'),
(3, 'Itaqui'),
(2, 'Caçapava do Sul'),
(4, 'Jaguarão'),
(6, 'São Borja');

INSERT INTO `cnh` (`idcnh`, `tipo`) VALUES
(7, 'AD'),
(3, 'D'),
(2, 'C'),
(5, 'AB'),
(4, 'E'),
(6, 'AC'),
(0, 'A'),
(8, 'AE'),
(1, 'B');

INSERT INTO `cor` (`idcor`, `cor`) VALUES
(1, 'Preto'),
(0, 'Branco'),
(2, 'Outra');