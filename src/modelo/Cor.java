package modelo;

public class Cor {

    public int cor;
    public static final int BRANCO = 0;
    public static final int PRETO = 1;
    public static final int OUTRO = 2;
   
    public Cor(int cor) {
       this.cor=cor;
    }

   

    /**
     * @return the cor
     */
    public int getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(int cor) {
        this.cor = cor;
    }
}
