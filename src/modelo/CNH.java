package modelo;

public class CNH {

    private String numero;
    private int tipo;
    public static final int A = 0;
    public static final int B = 1;
    public static final int C = 2;
    public static final int D = 3;
    public static final int E = 4;
    public static final int AB = 5;
    public static final int AC = 6;
    public static final int AD = 7;
    public static final int AE = 8;

    public CNH(String numero, int tipo) {
        this.numero = numero;
        this.tipo = tipo;
    }

    CNH() {
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
