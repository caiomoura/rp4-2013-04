package modelo;
import banco.de.dados.BDMotorista;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;


public class Veiculo {
    protected static final banco.de.dados.BDVeiculo bd = new banco.de.dados.BDVeiculo();
    private String placa;
    private Cor cor;
    private int km;
    private int capacidade;
    private Campus campus;
    private int ano;
    private String marca;
    private String modelo;
    private boolean status;
    

    public Veiculo() {
        status = true;
    }
    public boolean campoNulo() {
        if (placa == null || placa.isEmpty() )
                return true;
        return false;
    }
    
 
 
   public boolean inserir() {
        if (verfCampus()
                && validarPlaca(getPlaca())
                && validaCor(getCor())
                && validarAno(getAno())
                && validarCapacidade(getCapacidade())
                && validarKm(getKm())
                ) {
            bd.conectar();
            boolean ret = bd.criarVeiculo(this);
            bd.fechar();
            return ret;
        }
        return false;
    }
   
   
     public boolean alterar() {
        if (!verfCampus()
                || !validarPlaca(getPlaca())
                || !validaCor(getCor())
                || !validarAno(getAno())
                || !validarCapacidade(getCapacidade())
                || !validarKm(getKm())
                ) {
            return false;
        }

        bd.conectar();
        boolean ret = bd.alterar(this);
        bd.fechar();
        return ret;
    }
   
    public boolean desativar() {
        if (verfViagens() > 0) {
            return false;
        }

        setStatus(false);
        bd.conectar();
        boolean ret = bd.desativar(this);
        bd.fechar();
        
        return ret;
    }
   
     public boolean resgatar() {

        bd.conectar();
        Veiculo aux = bd.obterPorPlaca(getPlaca());
        bd.fechar();
        if (aux == null) {
            return false;
        }
        this.setCampus(aux.getCampus());
        this.setCor(aux.getCor());
        this.setAno(aux.getAno());
        this.setCapacidade(aux.getCapacidade());
        this.setKm(aux.getKm());
        this.setPlaca(aux.getPlaca());
        this.setMarca(aux.getMarca());
        this.setModelo(aux.getModelo());
        this.setStatus(aux.isStatus());
        return true;
    }
   

    public boolean validarPlaca(String placa) {
        
        if (placa == null)
            return false;

        if (placa.length() == 7) {

            for (int i = 0; i < 3; i++) {
                boolean vLetra = Character.isLetter(placa.charAt(i));

                if (vLetra == false) {
                    return false;
                }
            }

            for (int i = 3; i < 7; i++) {
                boolean vNumero = Character.isDigit(placa.charAt(i));

                if (vNumero == false) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Valida o ano de um veiculo
     *
     * @return <i>false</i> se menor que 1950, <i>true</i> se maior que 1950
     */
    public boolean validarAno(int ano) {
        if (ano < 1950 || ano > new Date().getYear() + 1900) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Valida a quilometragem do veiculo
     *
     * @return <i>false</i> se negativa, <i>true</i> se positiva
     */
    public boolean validarKm(int km) {
        if (km < 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Valida a capacidade do veiculo
     *
     * @return <i>false</i> se capacidade maior que 2, <i>true</i> se nÃ£o.
     */
    public boolean validarCapacidade(int capacidade) {

        if (capacidade < 2) {
            return false;
        } else {
            return true;
        }
    }
/**
     * Valida a cor do veiculo, se está entre os padrões {0,1,2}
     *
     * @return <i>false</i> se invalida, <i>true</i> se valida.
     */
   
    public boolean validaCor(Cor c) {
        return (c.getCor() >= 0 && c.getCor() < 3);
    }

       public boolean verfCampus() {
        if (getCampus() == null || getCampus().getId() < 0 || getCampus().getId() > 9)
        { return false;}
        return true;
    }
    
    private int verfViagens() {
        return Viagem.bdviagem.obterViagensPorVeiculo(getPlaca()).size();
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the cor
     */
    public Cor getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(Cor cor) {
        this.cor = cor;
    }

    /**
     * @return the km
     */
    public int getKm() {
        return km;
    }

    /**
     * @param km the km to set
     */
    public void setKm(int km) {
        this.km = km;
    }

    /**
     * @return the capacidade
     */
    public int getCapacidade() {
        return capacidade;
    }

    /**
     * @param capacidade the capacidade to set
     */
    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    /**
     * @return the campus
     */
    public Campus getCampus() {
        return campus;
    }

    /**
     * @param campus the campus to set
     */
    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }
    
     
    
    }
    
    

