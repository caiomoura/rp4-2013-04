package modelo;

import banco.de.dados.MockBDViagens;
import java.util.Date;
import java.util.Properties;
import static modelo.Usuario.bd;

public class Motorista extends Usuario {

    private int altdatacontr = 0;
    private int altstatus = 0;
    private int altcnh = 0;
    private Date datacontr;
    private boolean status;
    private CNH cnh;

    public Motorista() {
        cnh = new CNH();
        senha = gerarSenha();
        status = true;
    }

    public boolean inserir() {
        if (verfCampus()
                && verfCnh()
                && verfCpf()
                && verfDatanasc()
                && verfDatacontr()
                && verfEmail()
                && verfLogin()
                && verfSenha()
                && verfNome()) {
            bd.conectar();
            boolean ret = bd.criar(this);
            bd.fechar();
            return ret;
        }
        return false;
    }

    public boolean desativar() {
        if (verfViagens() > 0) {
            return false;
        }

        setStatus(false);
        bd.conectar();
        boolean ret = bd.desativar(this);
        bd.fechar();

        return ret;
    }

    public boolean resgatar() {

        altcnh = 1;
        altdatacontr = 1;
        altstatus = 1;
        altcampus = 1;
        altcpf = 1;
        altdatanac = 1;
        altemail = 1;
        altlogin = 1;
        altnome = 1;
        altsenha = 1;

        bd.conectar();
        Motorista aux = bd.obterPorCPF(cpf);
        bd.fechar();
        if (aux == null) {
            return false;
        }
        this.setCampus(aux.getCampus());
        this.setCnh(aux.getCnh());
        this.setCpf(aux.getCpf());
        this.setDatacontr(aux.getDatacontr());
        this.setDatanasc(aux.getDatanasc());
        this.setEmail(aux.getEmail());
        this.setLogin(aux.getLogin());
        this.setNome(aux.getNome());
        this.setSenha(aux.getSenha());
        this.setStatus(aux.getStatus());
        return true;
    }

    public boolean alterar() {
        if (!verfCampus()
                || !verfCnh()
                || !verfDatanasc()
                || !verfDatacontr()
                || !verfEmail()
                || !verfSenha()
                || !verfNome()
                || altcpf > 1
                || altlogin > 1) {
            return false;
        }

        bd.conectar();
        boolean ret = bd.alterar(this);
        bd.fechar();
        return ret;
    }

    public static String gerarSenha() {
        char[] s = new char[8];
        for (int i = 7; i >= 0; i--) {
            int a = (int) (Math.random() * i);
            char c = 0;
            switch (i) {
                case 7:
                    c = (char) (0x61 - 6 + (int) (Math.random() * 26));
                    break;
                case 6:
                    c = (char) (0x41 + (int) (Math.random() * 26));
                    break;
                case 5:
                    c = (char) (0x21 + (int) (Math.random() * 22));
                    if ((int) c > 0x2F) {
                        c += 10;
                    }
                    break;
                case 4:
                    c = (char) (0x30 + (int) (Math.random() * 10));
                    break;
                default:
                    c = (char) (0x21 + (int) (Math.random() * 84));
                    break;
            }
            if ((int) c > 0x5B) {
                c += 6;
            }
            while ((int) s[a] != 0) {
                a++;
            }
            s[a] = c;
        }
        return String.valueOf(s);
    }

    /**
     * @return the datacontr
     */
    public Date getDatacontr() {
        return datacontr;
    }

    /**
     * @param datacontr the datacontr to set
     */
    public void setDatacontr(Date datacontr) {
        altdatacontr++;
        this.datacontr = datacontr;
    }

    private boolean verfDatacontr() {
        Date aux = (Date) this.datanasc.clone();
        aux.setYear(aux.getYear() + 18);
        if (datacontr == null || datacontr.before(aux) || datacontr.after(new Date())) {
            return false;
        }
        return true;
    }

    /**
     * @return the ativo
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * @param ativo the ativo to set
     */
    private void setStatus(boolean ativo) {
        altstatus++;
        this.status = ativo;
    }

    /**
     * @return the cnh
     */
    public CNH getCnh() {
        return cnh;
    }

    /**
     * @param cnh the cnh to set
     */
    public void setCnh(CNH cnh) {
        altcnh++;
        this.cnh = cnh;
    }

    private boolean verfCnh() {
        try {
            Long.parseLong(cnh.getNumero());
        } catch (NumberFormatException e) {
            return false;
        }
        if (cnh.getNumero() == null
                || cnh.getNumero().length() != 11
                || cnh.getTipo() < 0
                || cnh.getTipo() > 8) {
            return false;
        }
        return true;
    }

    private boolean verfNome() {
        if (nome == null || nome.equals("")) {
            return false;
        }
        return true;
    }

    private int verfViagens() {
        return Viagem.bdviagem.obterViagensPorMotorista(cpf).size();
    }

    @Override
    public boolean campoNulo() {
        if (super.campoNulo()
                || cnh == null
                || cnh.getNumero() == null
                || cnh.getNumero().isEmpty()) {
            return false;
        }
        return true;
    }
}