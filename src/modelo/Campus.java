package modelo;

public class Campus {

    private int campus;
    
    public static final int ALEGRETE = 0;
    public static final int BAGE = 1;
    public static final int CACAPAVADOSUL = 2;
    public static final int DOMPEDRITO = 3;
    public static final int ITAQUI = 4;
    public static final int JAGUARAO = 5;
    public static final int SANTANADOLIVRAMENTO = 6;
    public static final int SAOBORJA = 7;
    public static final int SAOGABRIEL = 8;
    public static final int URUGUAIANA = 9;

    public Campus(int campus) {
        this.campus = campus;
    }

    /**
     * @return the campus
     */
    public int getId() {
        return campus;
    }

    /**
     * @param campus the campus to set
     */
    public void setId(int campus) {
        this.campus = campus;
    }
}
