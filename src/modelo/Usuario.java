package modelo;

import banco.de.dados.BDMotorista;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;

public class Usuario {
    
    protected int altcpf = 0;
    protected int altlogin = 0;
    protected int altnome = 0;
    protected int altcampus = 0;
    protected int altsenha = 0;
    protected int altemail = 0;
    protected int altdatanac = 0;

    protected static final banco.de.dados.BDMotorista bd = new banco.de.dados.BDMotorista();
    protected String cpf;
    protected String login;
    protected String nome;
    protected Campus campus;
    protected String senha;
    protected String email;
    protected Date datanasc;
    private boolean admin;

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        altcpf++;
        this.cpf = cpf;
    }

    public boolean verfCpf() {
        if (cpf == null
                || cpf.equals("00000000000") || cpf.equals("11111111111")
                || cpf.equals("22222222222") || cpf.equals("33333333333")
                || cpf.equals("44444444444") || cpf.equals("55555555555")
                || cpf.equals("66666666666") || cpf.equals("77777777777")
                || cpf.equals("88888888888") || cpf.equals("99999999999")
                || (cpf.length() != 11)) {
            return false;
        }

        char dig10, dig11;
        int sm, i, r, num, peso;

        try {
            Long.parseLong(cpf);
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {

                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            } else {
                dig10 = (char) (r + 48);
            }

            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (cpf.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            } else {
                dig11 = (char) (r + 48);
            }

            if (!(dig10 == cpf.charAt(9)) || !(dig11 == cpf.charAt(10))) {
                return false;
            }
        } catch (InputMismatchException erro) {
            return false;
        } catch (NumberFormatException e) {
            System.err.println("CPF deve ser composto apenas de números");
            return false;
        }
        BDMotorista bd = new BDMotorista();
        bd.conectar();
        if (bd.obterPorCPF(cpf) == null) {
            bd.fechar();
            return true;
        }
        bd.fechar();
        return false;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }
    
    public boolean verfLogin() {
        BDMotorista bd = new BDMotorista();
        bd.conectar();
        if (bd.obterPorLogin(login) == null) {
            bd.fechar();
            return true;
        }
        bd.fechar();
        return false;
    }
    
    public void setLogin(String login) {
        altlogin++;
        this.login = login;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        altnome++;
        this.nome = nome;
    }

    /**
     * @return the campus
     */
    public Campus getCampus() {
        return campus;
    }

    /**
     * @param campus the campus to set
     */
    public void setCampus(Campus campus) {
        altcampus++;
        this.campus = campus;
    }
    
    public boolean verfCampus() {
        if (campus == null || campus.getId() < 0 || campus.getId() > 9)
            return false;
        return true;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        altsenha++;
        this.senha = senha;
    }
    
    public static boolean verfSenha(String senha) {
        if (senha == null || senha.length() > 10 || senha.length() < 6) {
            return false;
        }
        boolean num = false,
                up = false,
                low = false;
        char[] c = senha.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (Character.isDigit(c[i])) {
                num = true;
            }
            if (Character.isLowerCase(c[i])) {
                low = true;
            }
            if (Character.isUpperCase(c[i])) {
                up = true;
            }
        }
        if (num && up && low) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean verfSenha() {
        if (senha == null || senha.length() > 10 || senha.length() < 6) {
            return false;
        }
        boolean num = false,
                up = false,
                low = false;
        char[] c = senha.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (Character.isDigit(c[i])) {
                num = true;
            }
            if (Character.isLowerCase(c[i])) {
                low = true;
            }
            if (Character.isUpperCase(c[i])) {
                up = true;
            }
        }
        if (num && up && low) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    
    public boolean verfEmail() {
        if (email == null || email.isEmpty()) {
            return false;
        }
        if (email.contains("@")) {
            String[] aux = email.split("@");
            if (aux.length > 2 || aux[0].isEmpty() || aux[1].isEmpty()) {
                return false;
            }
            if (!aux[1].contains(".") || aux[1].startsWith(".") || aux[1].endsWith(".")) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    public void setEmail(String email) {
        altemail++;
        this.email = email;
    }

    /**
     * @return the datanasc
     */
    public Date getDatanasc() {
        return datanasc;
    }

    /**
     * @param datanasc the datanasc to set
     */
    public void setDatanasc(Date datanasc) {
        altdatanac++;
        this.datanasc = datanasc;
    }
    
    public boolean verfDatanasc() {
        Date aux = new Date();
        aux.setYear(aux.getYear() - 18);
        if (datanasc == null || datanasc.after(aux)) {
            return false;
        }
        return true;
    }

    /**
     * retorna se o usúario é um administrador
     *
     * @return the admin
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * @param admin the admin to set
     */
    public void setAdmin() {
        this.admin = true;
    }
    
    public boolean campoNulo() {
        if (cpf == null || cpf.isEmpty() ||
                login == null || login.isEmpty() ||
                nome == null || nome.isEmpty() ||
                senha == null || senha.isEmpty() ||
                email == null || email.isEmpty() ||
                datanasc == null) return false;
        return true;
    }
}