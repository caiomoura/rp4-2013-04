/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banco.de.dados;

import java.util.ArrayList;
import modelo.Viagem;

/**
 *
 * @author Caio Alexandre
 */
public class MockBDViagens {
    
    private ArrayList<Viagem> viagens;

    public MockBDViagens() {
        viagens = new ArrayList<Viagem>();
    }
    
    public ArrayList<Viagem> obterViagensPorMotorista(String cpf) {
        return viagens;
    }
     public ArrayList<Viagem> obterViagensPorVeiculo(String placa) {
        return viagens;
    }
    
    public void adicionarViagem() {
        viagens.add(new Viagem());
    }
    
    public void limpar() {
        viagens.clear();
    }
}