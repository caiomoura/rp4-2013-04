/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package banco.de.dados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Caio Alexandre
 */
public class ConFactory {

    
    private static final String URL = "jdbc:mysql://localhost:3306",
            NOME = "root", SENHA = "root";
    public static final int MYSQL = 0;
    private static final String MySQLDriver = "com.mysql.jdbc.Driver";

    public static Connection conexao(int banco) throws ClassNotFoundException, SQLException {
        switch (banco) {
            case MYSQL:
                Class.forName(MySQLDriver);
                break;
        }
        return DriverManager.getConnection(URL, NOME, SENHA);
    }
}
