package banco.de.dados;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import modelo.*;

public class BDMotorista extends BDAdmin {

    /**
     * Inclui um Motorista no banco de dados.
     * <br><br>
     * Não são adimisiveis campos nulos
     *
     * @param motorista <b>Motorista</b> - O Objeto do motorista a ser incluido
     * no banco de dados.
     * @return boolean - Status se houve ou não a inclusão do Motorista no banco
     * de dados
     */
    public boolean criar(Motorista m) {

        if (m == null || m.getCnh() == null
                || m.getCpf() == null
                || m.getDatacontr() == null
                || m.getDatanasc() == null
                || m.getEmail() == null
                || m.getLogin() == null
                || m.getNome() == null
                || m.getSenha() == null) {
            return false;
        }

        try {

            cmd = "INSERT INTO `rp4`.`usuario`"
                    + "(`cpf`,`login`,`email`,`nome`,`campus`,`senha`,`datanasc`)"
                    + "VALUES(?,?,?,?,?,?,?);";


            comando = getConnection().prepareStatement(cmd);

            comando.setString(1, m.getCpf());
            comando.setString(2, m.getLogin());
            comando.setString(3, m.getEmail());
            comando.setString(4, m.getNome());
            comando.setInt(5, m.getCampus().getId());
            comando.setString(6, m.getSenha());
            comando.setDate(7, new Date(m.getDatanasc().getTime()));

            comando.execute();

            cmd = "INSERT INTO `rp4`.`motorista`"
                    + "(`cpf`,`status`,`numerocnh`,`tipocnh`,`datacontr`)"
                    + "VALUES(?,?,?,?,?);";

            comando = getConnection().prepareStatement(cmd);

            comando.setString(1, m.getCpf());
            comando.setString(2, String.valueOf(m.getStatus()));
            comando.setString(3, m.getCnh().getNumero());
            comando.setInt(4, m.getCnh().getTipo());
            comando.setDate(5, new Date(m.getDatacontr().getTime()));

            comando.execute();
            comando.close();

            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * Retorna a lista completa de todos os Motoristas no Banco de Dados.
     *
     * @return ArrayList - Lista de <b>Motoristas</b>.
     */
    public ArrayList<Motorista> obterTodos() {

        ArrayList<Motorista> lista = new ArrayList<Motorista>();

        try {

            cmd = "SELECT "
                    + "usuario.cpf,login,email,nome,campus,senha,datanasc,"
                    + "datacontr,status,numerocnh,tipocnh FROM rp4.usuario "
                    + "JOIN rp4.motorista ON motorista.cpf = usuario.cpf "
                    + "ORDER BY usuario.nome;";



            comando = getConnection().prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();

            while (rs.next()) {
                Motorista m = new Motorista();

                m.setCampus(new Campus(rs.getInt("campus")));
                m.setCnh(new CNH(rs.getString("numerocnh"), rs.getInt("tipocnh")));
                m.setCpf(rs.getString("cpf"));
                m.setDatacontr(rs.getDate("datacontr"));
                m.setDatanasc(rs.getDate("datanasc"));
                m.setEmail(rs.getString("email"));
                m.setLogin(rs.getString("login"));
                m.setNome(rs.getString("nome"));

                lista.add(m);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retorna a lista de Motoristas por Campus no Banco de Dados
     *
     * @param campus <b>Integer</b> - Numero referente Campus no Banco de Dados.
     * serem extraidos do Banco de Dados para o Motorista.
     * @return <b>ArrayList</b> - Lista de <b>Motorista</b>.
     */
    public ArrayList<Motorista> obterTodosPorCampus(int campus) {

        ArrayList<Motorista> lista = new ArrayList<Motorista>();

        try {

            cmd = "SELECT * FROM rp4.usuario JOIN rp4.motorista "
                    + "ON motorista.cpf = usuario.cpf ";

            if (campus != -1) {
                cmd += "WHERE `usuario`.`campus` = " + campus + " ";
            }

            cmd += "ORDER BY `usuario`.`nome`;";

            comando = con.prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();

            while (rs.next()) {
                Motorista m = new Motorista();

                m.setCampus(new Campus(rs.getInt("campus")));
                m.setCnh(new CNH(null, rs.getInt("tipocnh")));
                m.getCnh().setNumero(rs.getString("numerocnh"));
                m.getCnh().setTipo(rs.getInt("tipo"));
                m.setCpf(rs.getString("cpf"));
                m.setDatacontr(rs.getDate("datacontr"));
                m.setDatanasc(rs.getDate("datanasc"));
                m.setEmail(rs.getString("email"));
                m.setLogin(rs.getString("login"));
                m.setNome(rs.getString("nome"));

                lista.add(m);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retorna um Motoristas por CPF no Banco de Dados
     *
     * @param cpf <b>String</b> - Numero do CPF do Motorista Cadastrado.
     * serem extraidos do Banco de Dados para o Motorista.
     * @return <b>Motorista</b> - O Motorista do CPF solicitado,
     * <b><i>null</i></b> se não existe.
     */
    public Motorista obterPorCPF(String cpf) {

        try {

            cmd = "SELECT * FROM rp4.usuario JOIN rp4.motorista "
                    + "ON motorista.cpf = usuario.cpf ";

            cmd += "WHERE `usuario`.`cpf` = " + cpf + " ";

            cmd += "ORDER BY `usuario`.`nome`;";

            comando = con.prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();
            
            Motorista m = null;

            while (rs.next()) {
                m = new Motorista();
                
                m.setCampus(new Campus(rs.getInt("campus")));
                m.setCnh(new CNH(null, rs.getInt("tipocnh")));
                m.getCnh().setNumero(rs.getString("numerocnh"));
                m.getCnh().setTipo(rs.getInt("tipo"));
                m.setCpf(rs.getString("cpf"));
                m.setDatacontr(rs.getDate("datacontr"));
                m.setDatanasc(rs.getDate("datanasc"));
                m.setEmail(rs.getString("email"));
                m.setLogin(rs.getString("login"));
                m.setNome(rs.getString("nome"));
            }
            return m;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retorna um Motoristas por e-mail no Banco de Dados
     *
     * @param cpf <b>String</b> - E-mail do Motorista Cadastrado.
     * serem extraidos do Banco de Dados para o Motorista.
     * @return <b>Motorista</b> - O Motorista do e-mail solicitado,
     * <b><i>null</i></b> se não existe.
     */
    public Motorista obterPorEmail(String email) {

        try {

            cmd = "SELECT * FROM rp4.usuario JOIN rp4.motorista "
                    + "ON motorista.cpf = usuario.cpf ";

            cmd += "WHERE `usuario`.`email` = " + email + " ";

            cmd += "ORDER BY `usuario`.`nome`;";

            comando = con.prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();
            
            Motorista m = null;

            while (rs.next()) {
                m = new Motorista();

                m.setCampus(new Campus(rs.getInt("campus")));
                m.setCnh(new CNH(null, rs.getInt("tipocnh")));
                m.getCnh().setNumero(rs.getString("numerocnh"));
                m.getCnh().setTipo(rs.getInt("tipo"));
                m.setCpf(rs.getString("cpf"));
                m.setDatacontr(rs.getDate("datacontr"));
                m.setDatanasc(rs.getDate("datanasc"));
                m.setEmail(rs.getString("email"));
                m.setLogin(rs.getString("login"));
                m.setNome(rs.getString("nome"));
            }
            return m;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retorna um Motoristas por Nome de Usúario(login) no Banco de Dados
     *
     * @param cpf <b>String</b> - Nome de usúario do Motorista Cadastrado.
     * serem extraidos do Banco de Dados para o Motorista.
     * @return <b>Motorista</b> - O Motorista do login solicitado,
     * <b><i>null</i></b> se não existe.
     */
    public Motorista obterPorLogin(String login) {

        try {

            cmd = "SELECT * FROM rp4.usuario JOIN rp4.motorista "
                    + "ON motorista.cpf = usuario.cpf ";

            cmd += "WHERE `usuario`.`login` = " + login + " ";

            cmd += "ORDER BY `usuario`.`nome`;";

            comando = con.prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();
            
            Motorista m = null;

            while (rs.next()) {
                m = new Motorista();

                m.setCampus(new Campus(rs.getInt("campus")));
                m.setCnh(new CNH(null, rs.getInt("tipocnh")));
                m.getCnh().setNumero(rs.getString("numerocnh"));
                m.getCnh().setTipo(rs.getInt("tipo"));
                m.setCpf(rs.getString("cpf"));
                m.setDatacontr(rs.getDate("datacontr"));
                m.setDatanasc(rs.getDate("datanasc"));
                m.setEmail(rs.getString("email"));
                m.setLogin(rs.getString("login"));
                m.setNome(rs.getString("nome"));
            }
            return m;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Altera os dados de um Motoristas por CPF no Banco de Dados,
     *
     * @param cpf <b>String</b> - Numero do CPF do Motorista Cadastrado.
     * Banco de Dados para o Motorista.
     * @return <b>boolean</b> - Status se houve ou não a alteração do Motorista
     * no banco de dados
     */
    public boolean alterar(Motorista m) {

        try {
            cmd = "UPDATE `rp4`.`usuario` SET nome = ?, email = ?, datanasc = ?, campus = ?"
                    + " WHERE `usuario`.`cpf` = ?;";

            comando = getConnection().prepareStatement(cmd);
            comando.setString(1, m.getNome());
            comando.setString(2, m.getEmail());
            comando.setDate(3, new Date(m.getDatanasc().getTime()));
            comando.setInt(4, m.getCampus().getId());
            comando.setString(5, m.getCpf());

            if (comando.executeUpdate() == 0) {
                return false;
            }

            cmd = "UPDATE `rp4`.`motorista` SET status = ?, numerocnh = ?, tipocnh = ?"
                    + " WHERE `motorista`.`cpf` = ?;";

            comando = getConnection().prepareStatement(cmd);
            
            comando.setString(1, String.valueOf(m.getStatus()));
            comando.setString(2, m.getCnh().getNumero());
            comando.setInt(3, m.getCnh().getTipo());
            comando.setString(4, m.getCpf());
            
            if (comando.executeUpdate() == 0) {
                return false;
            }

            comando.close();

            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean desativar(Motorista m) {
        try {
            cmd = "UPDATE `rp4`.`motorista` SET status = ?"
                    + " WHERE `motorista`.`cpf` = ?;";

            comando = getConnection().prepareStatement(cmd);
            
            comando.setString(1, String.valueOf(m.getStatus()));
            comando.setString(2, m.getCpf());
            
            if (comando.executeUpdate() == 0) {
                return false;
            }

            comando.close();

            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}