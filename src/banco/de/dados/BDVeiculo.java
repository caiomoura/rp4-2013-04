package banco.de.dados;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Campus;
import modelo.Cor;
import modelo.Veiculo;

public class BDVeiculo extends BDAdmin {

    /**
     * Inclui um Veículo no banco de dados.
     *
     * @param veiculo <b>Veiculo</b> - O Objeto do veiculo a ser incluido no
     * banco de dados.
     * @return boolean - Status se houve ou não a inclusão do Motorista no banco
     * de dados
     */
    public boolean criarVeiculo(Veiculo veiculo) {


        try {

            cmd = "INSERT INTO `rp4`.`veiculo`"
                    + "(`placa`,`cor`,`km`,`capacidade`,`campus`,`ano`,`marca`,`modelo`)"
                    + "VALUES(?,?,?,?,?,?,?,?);";


            comando = getConnection().prepareStatement(cmd);

            comando.setString(1, veiculo.getPlaca());
            comando.setInt(2, veiculo.getCor().getCor());
            comando.setInt(3, veiculo.getKm());
            comando.setInt(4, veiculo.getCapacidade());
            comando.setInt(5, veiculo.getCampus().getId());
            comando.setInt(6, veiculo.getAno());
            comando.setString(7, veiculo.getMarca());
            comando.setString(8, veiculo.getModelo());

            System.out.println("Inserindo Veiculo no Banco de Dados!");

            comando.execute();

            comando.close();

            System.out.println("Veiculo Inserido no Banco de Dados");

            return true;
        } catch (SQLException e) {

            System.out.println("Erro de Conexão com o Banco de Dados!");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Retorna a lista completa de todos os Veiculos no Banco de Dados.
     *
     * @return ArrayList - Lista de <b>Veiculo</b>.
     *
     *
     *
     */
    public ArrayList<Veiculo> obterTodos() {

        ArrayList<Veiculo> lista = new ArrayList<Veiculo>();

        try {

            cmd = "SELECT "
                    + "placa,marca,modelo,ano,capacidadade,cor,km,campus FROM rp4.veiculo"
                    + "ORDER BY veiculo.ano;";


            comando = getConnection().prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();

            while (rs.next()) {
                Veiculo v = new Veiculo();

                v.setCampus(new Campus(rs.getInt("campus")));
                v.setCor(new Cor(rs.getInt("cor")));
                v.setPlaca(rs.getString("placa"));
                v.setKm(rs.getInt("km"));
                v.setCapacidade(rs.getInt("capacidade"));
                v.setModelo(rs.getString("modelo"));
                v.setMarca(rs.getString("marca"));
                v.setAno(rs.getInt("ano"));


                System.out.println("Adicionado " + v.getPlaca() + " a lista!");

                lista.add(v);
            }
            System.out.println("Seleção de veiculos concluida!");
            return lista;
        } catch (Exception e) {
            System.out.println("Erro de Conexão com o Banco de Dados!");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna a lista de Veiculos por Campus no Banco de Dados
     *
     * @param campus <b>Integer</b> - Numero referente Campus no Banco de Dados.
     * serem extraidos do Banco de Dados para o Veiculo.
     * @return <b>ArrayList</b> - Lista de <b>Veiculo</b>.
     */
    public ArrayList<Veiculo> obterTodosPorCampus(int campus) {

        ArrayList<Veiculo> lista = new ArrayList<Veiculo>();

        try {

            cmd = "SELECT * FROM rp4.veiculo";

            if (campus != -1) {
                cmd += "WHERE `veiculo`.`campus` = " + campus + " ";
            }

            cmd += "ORDER BY `veiculo`.`ano`;";

            comando = con.prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();

            while (rs.next()) {
                Veiculo v = new Veiculo();

                v.setCampus(new Campus(rs.getInt("campus")));
                v.setCor(new Cor(rs.getInt("cor")));
                v.setKm(rs.getInt("cpf"));
                v.setMarca(rs.getString("datacontr"));
                v.setModelo(rs.getString("modelo"));
                v.setCapacidade(rs.getInt("capacidade"));
                v.setAno(rs.getInt("ano"));
                v.setPlaca(rs.getString("placa"));

                lista.add(v);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Retorna um Veiculo por placa no Banco de Dados
     *
     * @param placa <b>String</b> - Numero da placa do Veiculo Cadastrado. serem
     * extraidos do Banco de Dados para o Motorista.
     * @return <b>Veiculo</b> - O Veiculo da placa solicitada,
     * @return <b><i>null</i></b> se não existir tal veiculo no banco de dados.
     */
    public Veiculo obterPorPlaca(String placa) {

        try {

            cmd = "SELECT * FROM rp4.veiculo";

            cmd += "WHERE `veiculo`.`placa` = " + placa + " ";



            comando = con.prepareStatement(cmd);

            ResultSet rs = comando.executeQuery();

            Veiculo v = null;

            while (rs.next()) {
                v = new Veiculo();

                v.setCampus(new Campus(rs.getInt("campus")));
                v.setCor(new Cor(rs.getInt("cor")));
                v.setKm(rs.getInt("cpf"));
                v.setMarca(rs.getString("datacontr"));
                v.setModelo(rs.getString("modelo"));
                v.setCapacidade(rs.getInt("capacidade"));
                v.setAno(rs.getInt("ano"));
                v.setPlaca(rs.getString("placa"));
            }
            return v;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Altera os dados de um Veiculo por placa no Banco de Dados,
     *
     * @param placa <b>String</b> - Numero da placa do Veiculo Cadastrado. Banco
     * de Dados para o Veiculo.
     * @return <b>true</b> - Se alterar os dados
     * @return <b>false</b> se não houve alteração no banco de dados
     */
    public boolean alterar(Veiculo v) {

        try {
            cmd = "UPDATE `rp4`.`veiculo` SET cor = ?, km = ?, capacidade = ?, campus = ?, ano = ?, marca = ?, modelo = ?"
                    + " WHERE `veiculo`.`placa` = ?;";

            comando = getConnection().prepareStatement(cmd);

            comando.setInt(1, v.getCor().getCor());
            comando.setInt(2, v.getKm());
            comando.setInt(3, v.getCapacidade());
            comando.setInt(4, v.getCampus().getId());
            comando.setInt(5, v.getAno());
            comando.setString(6, v.getMarca());
            comando.setString(7, v.getModelo());
            comando.setString(8, v.getPlaca());


            if (comando.executeUpdate() == 0) {
                return false;
            }


            comando.close();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Verifica se a placa já existe no banco de dados
     *
     * @return <i>false</i> se já existir, <i>true</i> se não existe
     */
    public boolean verificaPlacaExistente(String placa) {

        try {
            cmd = "SELECT placa FROM `rp4`.`veiculo` WHERE `veiculo`.`placa` = '" + placa + "';";


            comando = getConnection().prepareStatement(cmd);
            ResultSet rs = comando.executeQuery();

            if (rs.first() == false) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {

            System.out.println("Erro de Conexão com o Banco de Dados!");
            e.printStackTrace();
            return false;
        }


    }

    /**
     * Verifica se o veiculo possui viagens
     *
     * @return <i>false</i> se possuir, <i>true</i> se não possuir
     */
    public boolean verificaViagensExistentes(String placa) {

        try {
            cmd = "SELECT placa FROM `rp4`.`viagem` WHERE `veiculo`.`placa` = '" + placa + "';";

            comando = getConnection().prepareStatement(cmd);
            ResultSet rs = comando.executeQuery();

            if (rs.first() == false) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {

            System.out.println("Erro de Conexão com o Banco de Dados!");
            e.printStackTrace();
            return false;
        }

    }

    public boolean desativar(Veiculo v) {
        try {
            cmd = "UPDATE `rp4`.`veiculo` SET status = ?"
                    + " WHERE `veiculo`.`placa` = ?;";

            comando = getConnection().prepareStatement(cmd);

            comando.setString(1, String.valueOf(v.isStatus()));
            comando.setString(2, v.getPlaca());

            if (comando.executeUpdate() == 0) {
                return false;
            }

            comando.close();

            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}