package banco.de.dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BDAdmin {
    
    protected Connection con;
    protected PreparedStatement comando;
    protected String cmd;

    /**
     * Retorna se um login e senha conferem para entrar no sistema
     *
     * @param login <b>String</b> - Login do usuáruio a testar
     * @param senha <b>String</b> - Senha para conferir
     * @return <b></b> - Se a senha confere
     */
    public boolean checarSenha(String login, String senha) {
        
        if (login == null || senha == null)
            return false;
        
        if (login.equals("admin")) {
            
            try {

                cmd = "SELECT `configuracoes`.`valor` FROM `rp4`.`configuracoes` WHERE `configuracoes`.`parametro` = 'senha';";
                
                comando = con.prepareStatement(cmd);

                ResultSet rs = comando.executeQuery();

                while (rs.next()) {

                    if (senha.equals(rs.getString("valor"))){
                        fechar();
                        return true;
                    } else {
                        fechar();
                        return false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        } else {

            try {

                cmd = "SELECT `usuario`.`senha` FROM `rp4`.`usuario` WHERE `usuario`.`login` = ?;";

                comando = con.prepareStatement(cmd);

                comando.setString(1, login);

                ResultSet rs = comando.executeQuery();

                while (rs.next()) {

                    if (senha.equals(rs.getString("senha"))){
                        fechar();
                        return true;
                    } else {
                        fechar();
                        return false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Abre uma conexão com o servidor de banco de Dados SQL indicado
     * Servidor do tipo MySQL
     * @param url - URL do servidor
     * @param nome - Nome do usúario do servidor de Banco de Dados
     * @param senha  - senha do usúario do servidor de Banco de Dados
     */
    public void conectar() {
        try {
            this.setConnection(ConFactory.conexao(ConFactory.MYSQL));
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {            
            ex.printStackTrace();
        }
    }

    /**
     * Fecha a conexão existente com o Banco de Dados
     */
    public void fechar() {
        try {
            if (comando != null)
                comando.close();
            getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(BDMotorista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return A conexão com o servidor que esta sendo usada
     */
    public Connection getConnection() {
        return con;
    }

    /**
     * @param con A conexão que deseja-se usar;
     */
    public void setConnection(Connection con) {
        this.con = con;
    }
}