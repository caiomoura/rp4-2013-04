/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import banco.de.dados.ConFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import modelo.CNH;
import modelo.Campus;
import modelo.Motorista;
import modelo.Usuario;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author unipampa
 */
public class Modulo2Incluir {
    
    Motorista m1;
    
    private final Campus CAMPUS1 = new Campus(Campus.ALEGRETE);
    private final CNH CNH0 = new CNH("02251050681", CNH.C);
    private final CNH CNH1 = new CNH("05521020981", CNH.AB);
    private final CNH CNH2 = new CNH("OSSZ1OZ0681", CNH.AB);
    private final CNH CNH3 = new CNH("0552102098", CNH.AB);
    private final CNH CNH4 = new CNH("055210209811", CNH.AB);
    private final CNH CNH5 = new CNH("05521020981", -1);
    private final CNH CNH6 = new CNH("05521020981", 9);
    private final String CPF0 = "02432299017";
    private final String CPF1 = "02485674035";
    private final String CPF2 = "OZ48S674OES";
    private final String CPF3 = "02486574021";
    private final Date DATAC1 = new Date(2012-1900, 6, 5);
    private final Date DATAC2 = new Date(2014-1900, 6, 5);
    private final Date DATAN1 = new Date(1993-1900, 5, 10);
    private final Date DATAN2 = new Date(1998-1900, 5, 10);
    private final String EMAIL0 = "emailreserva@gmail.com";
    private final String EMAIL1 = "caio.herter@gmail.com";
    private final String EMAIL2 = "caio.herter.gmail.com";
    private final String EMAIL3 = "caio@herter@gmail.com";
    private final String LOGIN0 = "LoginReserva";
    private final String LOGIN1 = "TheLinthus";
    private final String NOME1 = "Caio Alexandre Herter de Moura";
    
    public Modulo2Incluir() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * cria um motorista de atributos validos para se usar como template
     */
    @Before
    public void setUp() throws ClassNotFoundException, SQLException {
        m1 = new Motorista();
        m1.setCampus(CAMPUS1);
        m1.setCnh(CNH1);
        m1.setCpf(CPF1);
        m1.setDatacontr(DATAC1);
        m1.setDatanasc(DATAN1);
        m1.setEmail(EMAIL1);
        m1.setLogin(LOGIN1);
        m1.setNome(NOME1);
        
        Connection con = ConFactory.conexao(ConFactory.MYSQL);
        System.out.println("Preparando Banco de Dados");
        con.prepareStatement("delete from rp4.usuario;").execute();
        
        /////////////////////LIMPAR BANCO DE DADOS PARA EXECUTAR NOVOS TESTES///////////////////////
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void tudoValido() {
        System.out.println("Motorista com Todos os campos Validos");
        assertTrue(m1.inserir());
    }
    
    @Test
    public void SucessoGeracaoDeSenha() {
        assertTrue(Usuario.verfSenha(Motorista.gerarSenha()));
    }
    
    /**
     * Testa a insersão de Motorista com CPF nulo
     */
    @Test
    public void CPFnulo() {
        System.out.println("Motorista com CPF Nulo.");
        Motorista m1 = new Motorista();
        m1.setCpf(null); //cpf nulo
        assertFalse(m1.inserir());
    }
    
    /**
     * Testa a insersão de Motorista com CPF contendo letras
     */
    @Test
    public void CPFcomLetras() {
        System.out.println("Motorista com CPF contendo Letras.");
        m1.setCpf(CPF2); //cpf com letras inserido
        assertFalse(m1.inserir());
    }
    
    /**
     * Testa a insersão de Motorista com CPF cotendo digito Verificador invalido
     */
    @Test
    public void CPFdigitoVerificador() {
        System.out.println("Motorista com CPF com o digito verificador invalido.");
        m1.setCpf(CPF3); //cpf com digito verificador errado inserido
        assertFalse(m1.inserir());
    }
    
    /**
     * Testa a insersão de Motorista com CPF contendo menos de 11 digitos
     */
    @Test
    public void CPFmenosDe11Digitos() {
        System.out.println("Motorista com CPF Menos de 11 digitos.");
        m1.setCpf(CPF1.substring(1)); //mesmo cpf de CPF1 porém sem o primeiro digito é inserido (10 digitos)
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CPFmaisDe11Digitos() {
        System.out.println("Motorista com CPF Mais de 11 digitos.");
        m1.setCpf(CPF1+"5"); //mesmo cpf de CPF1 porém com mais um digito é inserido (12 digitos)
        assertFalse(m1.inserir());
        
    }
    
    /**
     * Testa a insersão de Motorista com CPF Duplicado.
     */
    @Test
    public void CPFduplicado() {
        System.out.println("Motorista com CPF Duplicado.");
        
        m1.inserir(); //insere m1 para vereficar repetição futuramente
        
        m1.setCnh(CNH0); //nova cnh para não cair erro de CNH repetida
        m1.setEmail(EMAIL0); //novo email para não cair erro de email repeitdo
        m1.setLogin(LOGIN0); //novo login para não cair erro de login repetido
        //não modificar o CPF para cair no erro de CPF repetido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CNHduplicada() {
        System.out.println("Motorista com CNH Duplicada.");
        
        m1.inserir(); //insere m1 para vereficar repetição futuramente
        
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setEmail(EMAIL0); //novo email para não cair erro de email repeitdo
        m1.setLogin(LOGIN0); //novo login para não cair erro de login repetido
        //não modificar o CNH para cair no erro de CNH repetido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CNHnula() {
        System.out.println("Motorista com CNH nula");
        m1.getCnh().setNumero(null); //
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CNHcomLetras() {
        System.out.println("Motorista com CNH com letras");
        m1.setCnh(CNH2); //cnh com letras inserido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CNHmenosDe11Digitos() {
        System.out.println("Motorista com CNH menos de 11 digitos");
        m1.setCnh(CNH3); //cnh com letras inserido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CNHmaisDe11Digitos() {
        System.out.println("Motorista com CNH mais de 11 digitos");
        m1.setCnh(CNH4); //cnh com letras inserido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void TipoCNHinvalidoNegativo() {
        System.out.println("Motorista com CNH tipo errado negativo");
        m1.setCnh(CNH5); //cnh com letras inserido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void TipoCNHinvalidoNaoExiste() {
        System.out.println("Motorista com CNH tipo errado não existe");
        m1.setCnh(CNH6); //cnh com letras inserido
        assertFalse(m1.inserir());
    }
    
    @Test
    public void DataNascimentoInvalida() {
        System.out.println("Motorista com Data de nascimento Invalida");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setDatanasc(DATAN2);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void DataNascimentoNula() {
        System.out.println("Motorista com Data de nascimento nula");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setDatanasc(null);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void NomeNulo() {
        System.out.println("Motorista com Nome nulo");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setNome(null);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CampusDeOrigemInvalidoNegativo() {
        System.out.println("Motorista com Campus negativo");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setCampus(new Campus(-1));
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CampusDeOrigemInvalidoNaoExiste() {
        System.out.println("Motorista com Campus inexistente");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setCampus(new Campus(16));
        assertFalse(m1.inserir());
    }
    
    @Test
    public void CampusDeOrigemnulo() {
        System.out.println("Motorista com Campus nulo");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setCampus(null);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void DataContratacaoInvalida() {
        System.out.println("Motorista com data de Contratação invalida");
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setDatacontr(DATAC2);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void DataContratacaoNula() {
        System.out.println("Motorista com data de Contratação nula");
        m1.setDatacontr(null);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void EmailInvalido1Arroba() {
        System.out.println("Motorista com e-mail sem arroba");
        m1.setEmail(EMAIL2);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void EmailInvalidoMultiplosArrobas() {
        System.out.println("Motorista com e-mail com multiplos arrobas");
        m1.setEmail(EMAIL3);
        assertFalse(m1.inserir());
    }
    
    @Test
    public void EmailNulo() {
        System.out.println("Motorista com e-mail nulo");
        m1.setEmail(null);
        assertFalse(m1.inserir());
    }
}