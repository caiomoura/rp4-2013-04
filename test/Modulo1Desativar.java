
import banco.de.dados.ConFactory;
import java.sql.Connection;
import java.sql.SQLException;
import modelo.Campus;
import modelo.Veiculo;
import modelo.Viagem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Karine
 */
public class Modulo1Desativar {

    private final String PLACA = "AWCD123";
    private final Campus CAMPUS1 = new Campus(Campus.URUGUAIANA);
    Veiculo v1;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ClassNotFoundException, SQLException {
        v1 = new Veiculo();
        v1.setPlaca(PLACA);
        v1.setCampus(CAMPUS1);
        
        Connection con = ConFactory.conexao(ConFactory.MYSQL);
        System.out.println("Preparando Banco de Dados");
        con.prepareStatement("delete from rp4.veiculo;").execute();
        
        /////////////////////LIMPAR BANCO DE DADOS PARA EXECUTAR NOVOS TESTES///////////////////////
    }

    @After
    public void tearDown() {
    }

    @Test
    public void sucesso() {
        v1.setPlaca(PLACA);


        v1.inserir();
        Viagem.bdviagem.limpar();
        v1.desativar();
        v1.resgatar();
        assertFalse(v1.isStatus());
    }

    @Test
    public void comviagem() {
        v1.setPlaca(PLACA);


        v1.inserir();
        Viagem.bdviagem.adicionarViagem();
        v1.desativar();
        v1.resgatar();
        assertTrue(v1.isStatus());
    }

    @Test
    public void inexistente() {
        v1 = new Veiculo();
        v1.setPlaca(PLACA);

        Viagem.bdviagem.limpar();

        assertFalse(v1.desativar());
    }

    @Test
    public void camposnulos() {
        v1.setPlaca(PLACA);

        v1.inserir();
        Viagem.bdviagem.limpar();
        v1.desativar();
        v1.resgatar();
        assertFalse(v1.campoNulo());
    }
}
