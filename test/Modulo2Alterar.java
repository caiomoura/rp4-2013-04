/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import banco.de.dados.ConFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import modelo.CNH;
import modelo.Campus;
import modelo.Motorista;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Caio Alexandre
 */
public class Modulo2Alterar {
    
    Motorista m1;
    
    private final Campus CAMPUS1 = new Campus(Campus.BAGE);
    private final CNH CNH0 = new CNH("96385207410", CNH.C);
    private final CNH CNH1 = new CNH("74108520963", CNH.AB);
    private final CNH CNH2 = new CNH("OSSZ1OZ0681", CNH.AB);
    private final CNH CNH3 = new CNH("0552102098", CNH.AB);
    private final CNH CNH4 = new CNH("055210209811", CNH.AB);
    private final CNH CNH5 = new CNH("05521020981", -1);
    private final CNH CNH6 = new CNH("05521020981", 9);
    private final String CPF0 = "98516036782";
    private final String CPF1 = "73744545547";
    private final String CPF2 = "51782162615";
    private final Date DATAC1 = new Date(2012 - 1900, 9, 27);
    private final Date DATAC2 = new Date(2014-1900, 6, 5);
    private final Date DATAC0 = new Date(2012 - 1900, 9, 28);
    private final Date DATAN1 = new Date(1990 - 1900, 0, 9);
    private final Date DATAN2 = new Date(1998-1900, 5, 10);
    private final Date DATAN0 = new Date(1989 - 1900, 0, 9);
    private final String EMAIL0 = "emailreserva@hotmail.com";
    private final String EMAIL1 = "emailreserva2@hotmail.com";
    private final String EMAIL2 = "emailreserva3@hotmail.com";
    private final String LOGIN0 = "TheLoginReserva";
    private final String LOGIN1 = "TheLogin";
    private final String NOME1 = "Fulano de tal reserva pra testes";

    public Modulo2Alterar() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ClassNotFoundException, SQLException {
        m1 = new Motorista();
        m1.setCampus(CAMPUS1);
        m1.setCnh(CNH1);
        m1.setCpf(CPF1);
        m1.setDatacontr(DATAC1);
        m1.setDatanasc(DATAN1);
        m1.setEmail(EMAIL1);
        m1.setLogin(LOGIN1);
        m1.setNome(NOME1);
        
        Connection con = ConFactory.conexao(ConFactory.MYSQL);
        System.out.println("Preparando Banco de Dados");
        con.prepareStatement("delete from rp4.usuario;").execute();
        
        /////////////////////LIMPAR BANCO DE DADOS PARA EXECUTAR NOVOS TESTES///////////////////////
    }

    @After
    public void tearDown() {
    }

    @Test
    public void sucesso() {
        System.out.println("Altrar dados de Motorista com sucesso");
        m1.inserir(); //inserir m1 para futuramente editar
        
        m1.setCampus(new Campus(Campus.CACAPAVADOSUL));
        m1.setCnh(new CNH("32198700456", CNH.C));
        m1.setEmail("lollol@gmail.com");
        m1.setNome("Dino da Silva Sauro");
        m1.setSenha("456#TaG1");
        m1.setDatanasc(DATAN0);
        m1.setDatacontr(DATAC0);
        
        assertTrue(m1.alterar());
    }

    @Test
    public void falhaCpfInexiste() {
        System.out.println("Altrar dados de Motorista inexiste");
        
        m1.setCampus(new Campus(Campus.CACAPAVADOSUL));
        m1.setCnh(new CNH("32198700456", CNH.C));
        m1.setEmail("lollol@gmail.com");
        m1.setNome("Dino da Silva Sauro");
        m1.setSenha("456#TaG1");
        m1.setDatanasc(DATAN0);
        m1.setDatacontr(DATAC0);
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void camposnulos() {
        System.out.println("Verificação de Motorista com campos nulos");
        
        m1.inserir();
        
        m1.setCnh(CNH0);
        m1.setCpf(CPF0);
        m1.setEmail(EMAIL0);
        m1.setLogin(LOGIN0);
        
        m1.alterar();
        
        m1.resgatar();
        assertFalse(m1.campoNulo());
    }
    
    @Test
    public void CNHduplicada() {
        System.out.println("Motorista com CNH Duplicada.");
        
        m1.inserir(); //insere m1 para vereficar repetição futuramente
        
        m1.setCpf(CPF0); //novo CPF para não cair erro de cpf repetido
        m1.setEmail(EMAIL0); //novo email para não cair erro de email repeitdo
        m1.setLogin(LOGIN0); //novo login para não cair erro de login repetido
        m1.setCnh(CNH0); //nova CNH para não cair erro de CNH repetida
        
        m1.inserir(); //insere m1 (diferente) para alterar os dados
        m1.setCnh(CNH1); //coloca CNH do primeiro m1 para alterar
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CNHnula() {
        System.out.println("Motorista com CNH nula");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.getCnh().setNumero(null); //define CNH nula para alterar
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CNHcomLetras() {
        System.out.println("Motorista com CNH com letras");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCnh(CNH2); //cnh com letras inserido
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CNHmenosDe11Digitos() {
        System.out.println("Motorista com CNH menos de 11 digitos");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCnh(CNH3); //cnh com letras inserido
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CNHmaisDe11Digitos() {
        System.out.println("Motorista com CNH mais de 11 digitos");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCnh(CNH4); //cnh com letras inserido
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void TipoCNHinvalidoNegativo() {
        System.out.println("Motorista com CNH tipo errado negativo");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCnh(CNH5); //cnh com letras inserido
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void TipoCNHinvalidoNaoExiste() {
        System.out.println("Motorista com CNH tipo errado não existe");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCnh(CNH6); //cnh com letras inserido
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void DataNascimentoInvalida() {
        System.out.println("Motorista com Data de nascimento Invalida");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setDatanasc(DATAN2);
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void DataNascimentoNula() {
        System.out.println("Motorista com Data de nascimento nula");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setDatanasc(null);
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void NomeNulo() {
        System.out.println("Motorista com Nome nulo");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setNome(null);
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CampusDeOrigemInvalidoNegativo() {
        System.out.println("Motorista com Campus negativo");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCampus(new Campus(-1));
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CampusDeOrigemInvalidoNaoExiste() {
        System.out.println("Motorista com Campus inexistente");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCampus(new Campus(16));
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void CampusDeOrigemnulo() {
        System.out.println("Motorista com Campus nulo");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setCampus(null);
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void DataContratacaoInvalida() {
        System.out.println("Motorista com data de Contratação invalida");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setDatacontr(DATAC2);
        
        assertFalse(m1.alterar());
    }
    
    @Test
    public void DataContratacaoNula() {
        System.out.println("Motorista com data de Contratação nula");
        
        m1.inserir(); //insere m1 para alterar os dados
        
        m1.setDatacontr(null);
        
        assertFalse(m1.alterar());
    }
}
