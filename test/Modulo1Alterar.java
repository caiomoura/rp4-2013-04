
import banco.de.dados.ConFactory;
import java.sql.Connection;
import java.sql.SQLException;
import modelo.Campus;
import modelo.Cor;
import modelo.Motorista;
import modelo.Veiculo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Karine
 */
public class Modulo1Alterar {

    Veiculo v1;
    private final Campus CAMPUS1 = new Campus(Campus.ALEGRETE);
    private final Cor C0R0 = new Cor(0);
    private final Cor COR1 = new Cor(10);
    private final String PLACA = "AWD1233";
    private final String PLACA0 = "BCD1234";
    private final String PLACA1 = "A1CD123";
    private final String PLACA2 = "";
    private final String PLACA4 = "1233333";
    private final String PLACA5 = "ABCDEFG";
    private final String PLACA6 = "A1";
    private final String PLACA7 = "ABCD123FGH";
    private final int ANO0 = 2010;
    private final int ANO1 = 1;
    private final int ANO2 = 200313;
    private final int ANO3 = 1845;
    private final int ANO4 = 2020;
    private final int CAPACIDADE0 = 2;
    private final int CAPACIDADE1 = 0;
    private final int KM0 = 10000;
    private final int KM1 = -10000;
    private final String MARCA0 = "Chevrolet";
    private final String MODELO0 = "Montana";

    public Modulo1Alterar() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ClassNotFoundException, SQLException {
        v1 = new Veiculo();
        v1.setCampus(CAMPUS1);
        v1.setPlaca(PLACA);
        v1.setAno(ANO0);
        v1.setCapacidade(CAPACIDADE0);
        v1.setCor(C0R0);
        v1.setKm(KM0);
        v1.setMarca(MARCA0);
        v1.setModelo(MODELO0);

        Connection con = ConFactory.conexao(ConFactory.MYSQL);
        System.out.println("Preparando Banco de Dados");
        con.prepareStatement("delete from rp4.veiculo;").execute();

        /////////////////////LIMPAR BANCO DE DADOS PARA EXECUTAR NOVOS TESTES///////////////////////
    }

    @After
    public void tearDown() {
    }

    @Test
    public void sucesso() {
        System.out.println("Altrar dados do Veiculo com sucesso");
        v1.inserir();

        v1.setCampus(new Campus(Campus.CACAPAVADOSUL));
        v1.setAno(2012);
        v1.setCapacidade(3);
        v1.setCor(new Cor(Cor.OUTRO));
        v1.setKm(600);
        v1.setMarca("Volkswagen");
        v1.setModelo("Fusca");

        assertTrue(v1.alterar());
    }

    @Test
    public void falhaPlacaInexiste() {
        System.out.println("Alterar dados de Veiculo inexiste");

        v1.setCampus(new Campus(Campus.CACAPAVADOSUL));
        v1.setAno(2012);
        v1.setCapacidade(3);
        v1.setCor(new Cor(Cor.OUTRO));
        v1.setKm(600);
        v1.setMarca("Volkswagen");
        v1.setModelo("Fusca");

        assertFalse(v1.alterar());
    }

    @Test
    public void camposnulos() {
        System.out.println("Verificação de Veiculo com campos nulos");

        v1.inserir();

        v1.alterar();

        v1.resgatar();
        assertFalse(v1.campoNulo());
    }

    @Test
    public void kmInvalida() {
        System.out.println("km invalida (negativa)");
        v1.inserir();
        v1.setKm(KM1);
        assertFalse(v1.inserir());
    }

    @Test
    public void CorInvalida() {
        System.out.println("Cor invalida");
        v1.inserir();
        v1.setCor(COR1);
        assertFalse(v1.inserir());
    }

    @Test
    public void CapacidadeInvalida() {
        System.out.println("Capacidade <2");
        v1.inserir();
        v1.setCapacidade(CAPACIDADE1);
        assertFalse(v1.inserir());
    }

    @Test
    public void AnosSuperiorAtual() {
        System.out.println("Ano superior ao ano atual");
        v1.inserir();
        v1.setAno(ANO4);
        assertFalse(v1.inserir());
    }

    @Test
    public void AnoInferior1950() {
        System.out.println("Ano inferior a 1950");
        v1.inserir();
        v1.setAno(ANO3);
        assertFalse(v1.inserir());
    }

    @Test
    public void maior4digitosAno() {
        System.out.println("Ano com mais de 4 digitos");
        v1.inserir();
        v1.setAno(ANO2);
        assertFalse(v1.inserir());
    }

    @Test
    public void Menor4digitosAno() {
        System.out.println("Ano com menos 4 digitos");
        v1.inserir();
        v1.setAno(ANO1);
        assertFalse(v1.inserir());
    }

    @Test
    public void PlacaNula() {
        System.out.println("Veiculo com placa nula");
        v1.inserir();
        v1.setPlaca(null);
        assertFalse(v1.inserir());
    }

    @Test
    public void PlacaComDigitos() {
        System.out.println("Placa com digitos");
        v1.inserir();
        v1.setPlaca(PLACA4);
        assertFalse(v1.inserir());
    }

    @Test
    public void PlacaComLetras() {
        System.out.println("Placa com letras");
        v1.inserir();
        v1.setPlaca(PLACA5);

        assertFalse(v1.inserir());
    }

    @Test
    public void PlacaMenos7Caracter() {
        System.out.println("Placa com menos de 7 caracter");
        v1.inserir();
        v1.setPlaca(PLACA6);
        assertFalse(v1.inserir());
    }

    @Test
    public void PlacaMais7Caracter() {
        System.out.println("Placa com mais de 7 caracter");
        v1.inserir();
        v1.setPlaca(PLACA7);
        assertFalse(v1.inserir());
    }

    @Test
    public void Mais7Caracter() {
        System.out.println("Placa com mais de 7 caracter");
        v1.inserir();
        v1.setPlaca(PLACA7);
        assertFalse(v1.inserir());
    }
}
