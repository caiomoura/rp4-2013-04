/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import banco.de.dados.ConFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import modelo.CNH;
import modelo.Campus;
import modelo.Motorista;
import modelo.Usuario;
import modelo.Viagem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Caio Alexandre
 */
public class Modulo2Desativar {

    Motorista m1;
    private final Campus CAMPUS1 = new Campus(Campus.URUGUAIANA);
    private final CNH CNH1 = new CNH("12345678901", CNH.C);
    private final CNH CNH2 = new CNH("10987654321", CNH.B);
    private final CNH CNH3 = new CNH("20738346470", CNH.D);
    private final String CPF1 = "22687934518";
    private final String CPF2 = "31602375593";
    private final String CPF3 = "87740710687";
    private final String CPF4 = "20738346470";
    private final Date DATAC1 = new Date(2012 - 1900, 2, 17);
    private final Date DATAN1 = new Date(1991 - 1900, 11, 25);
    private final String EMAIL1 = "emailreserva@gmail.com";
    private final String EMAIL2 = "emailreserva2@gmail.com";
    private final String EMAIL3 = "emailreserva3@gmail.com";
    private final String LOGIN1 = "LoginReserva";
    private final String LOGIN2 = "LoginReserva2";
    private final String LOGIN3 = "LoginReserva3";
    private final String NOME1 = "Fulano Reserva para Excluir";

    public Modulo2Desativar() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ClassNotFoundException, SQLException {
        m1 = new Motorista();
        m1.setCampus(CAMPUS1);
        m1.setDatacontr(DATAC1);
        m1.setDatanasc(DATAN1);
        m1.setNome(NOME1);
        
        Connection con = ConFactory.conexao(ConFactory.MYSQL);
        System.out.println("Preparando Banco de Dados");
        con.prepareStatement("delete from rp4.usuario;").execute();
        
        /////////////////////LIMPAR BANCO DE DADOS PARA EXECUTAR NOVOS TESTES///////////////////////
    }

    @After
    public void tearDown() {
    }

    @Test
    public void sucesso() {
        System.out.println("Desativação de Motorista com sucesso");
        m1.setCnh(CNH1);
        m1.setCpf(CPF1);
        m1.setEmail(EMAIL1);
        m1.setLogin(LOGIN1);
        
        m1.inserir();
        Viagem.bdviagem.limpar(); //limpa a lista de viagens do Mock para que não haja viagens pendentes
        m1.desativar();
        m1.resgatar();
        assertFalse(m1.getStatus());
    }
    
    @Test
    public void comviagem() {
        System.out.println("Desativação de Motorista com viagens pendentes");
        m1.setCnh(CNH2);
        m1.setCpf(CPF2);
        m1.setEmail(EMAIL2);
        m1.setLogin(LOGIN2);
        
        m1.inserir();
        Viagem.bdviagem.adicionarViagem(); //adiciona viagem, para que o motorista tenha viagem pendente
        m1.desativar();
        m1.resgatar();
        assertTrue(m1.getStatus());
    }
    
    @Test
    public void inexistente() {
        System.out.println("Desativação de Motorista inexistente");
        m1 = new Motorista();
        m1.setCpf(CPF3); //inserir cpf para buscar (resgatar)
        //não precisa inserir outros valores porque não havera insersão logo não havera validaçõa de campos
        
        Viagem.bdviagem.limpar(); //limpa a lista de viagens do Mock para que não haja viagens pendentes
        
        assertFalse(m1.desativar());
    }
    
    @Test
    public void camposnulos() {
        System.out.println("Verificação de Motorista com campos nulos");
        m1.setCnh(CNH3);
        m1.setCpf(CPF4);
        m1.setEmail(EMAIL3);
        m1.setLogin(LOGIN3);
        
        m1.inserir();
        m1.resgatar();
        assertFalse(m1.campoNulo());
    }
}
